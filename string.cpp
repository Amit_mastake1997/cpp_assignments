/*
 * main.cpp
 *
 *  Created on: 06-Aug-2020
 *      Author: sunbeam
 */


#include<iostream>
#include<cstring>
#include <stdlib.h>
using namespace std;

class String
{
private:

	int length;
	char *buffer;

public:
	String()
{
		this->length=0;
		this->buffer=NULL;
}

	  void setLength(int length)
	  {
		  if(length<0)
		  throw "Length can not be negative";
		  this->length=length;
	  }
	  void create()
	   {
	 	  this->buffer=new char[length];
	   }
	  void accept()
	    {
	  	  char ch[length];
	  	  cout<<"Enter String:  "<<endl;
	        cin>>ch;
	        memcpy(buffer,&ch,length);
	    }
	    void display()
	    {
	  	  for(int i=0;i<length;i++)
	  	  {
	           cout<<buffer[i];
	  	  }
	    }
	  };
	  int main()
	  {
	  	try
	  	{
	  		   String st;

	  		   st.create();
	  		   st.setLength(5);
	  		   st.accept();
	  		   st.display();
	  	}
	  	catch(char const * messege)
	  	{
	  		cout<<messege;
	  	}
	  	return 0;
	  }
