/*
 * main.cpp
 *
 *  Created on: 21-Jul-2020
 *      Author: sunbeam
 */

#include<iostream>

using namespace std;
int count = 1000;
class Account
{
private:
	char name[ 50 ];
	int number;
	char type[ 50 ];
	float balance;
public:
   void accept_account_info( )
	{
	   cout<<"Name	:	";
	   cin>>this->name ;
		cout<<"Type	:	";
		cin>>this->type;
		cout<<"Balance	:	";
		cin>>this->balance ;
	}
	void print_account_info()
		{
		cout<<"Name :"<<this->name<<endl;
		cout<<"number:"<<this->number<<endl;
		cout<<"type:"<<this->type<<endl;
		cout<<"balance:"<<this->balance<<endl;
		}
		void accept_account_number( int &number )
		{
			cout<<"Account number	:	"<<endl;
			cin>> number ;
		}
		void accept_amount( float &amount )
		{
			cout<<"Amount	:	"<<endl;
			cin>> amount ;
		}
		void print_account_number( int number )
		{
			cout<<"Account number	:"<<number<<endl;
		}
		void print_balance( float balance )
		{
			cout<<"Balance	:"<<balance ;
		}

	float getBalance() const {
		return balance;
	}

	void setBalance(float balance) {
		this->balance = balance;
	}

	const char* getName() const {
		return name;
	}

	int getNumber() const {
		return number;
	}

	void setNumber(int number) {
		this->number = number;
	}

	const char* getType() const {
		return type;
	}
};
class Bank
{
private:
	  int index;
	  Account arr[ 5 ];
public:

	int create_account(Account &account)
		{
		  account.setNumber(++ count);

			this->arr[ ++ this->index] = account;
			return account.getNumber();
		}

	float deposit( int number, float amount )
	{
		for( int index = 0; index <= this->index; ++ index )
		{
			if(this->arr[ index ].getNumber()== number  )
			{
				float bal=this->arr[ index ].getBalance();
				bal+= amount;
				this->arr[ index ].setBalance(bal);
				return this->arr[ index ].getBalance();
			}
		}
		return 0;
	}
	float withdraw(  int number, float amount )
	{
		for( int index = 0; index <= this->index; ++ index )
		{
			if( this->arr[ index ].getNumber() == number  )
			{
				float bal=this->arr[ index ].getBalance();
				bal-= amount;
				this->arr[ index ].setBalance(bal);
				return this->arr[ index ].getBalance();
			}
		}
		return 0;
	}
	class Account get_account_details(int number )
	{
		struct Account account = { };
		for( int index = 0; index <= this->index; ++ index )
		{
			if( this->arr[ index ].getNumber() == number  )
			{
				account = this->arr[ index ];
			}
		}
		return account;
	}

	int getIndex() const {
		return index;
	}

	void setIndex(int index) {
		this->index = index;
	}
};


int menu_list( void )
{
	int choice;
	cout<<"\n0.Exit\n";
	cout<<"1.Create New Account\n";
	cout<<"2.Deposit\n";
	cout<<"3.Withdraw\n";
	cout<<"4.Print Account details\n";
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
int main( void )
{
	int choice;
	int accNumber;
	float balance, amount;
	Bank bank ;
	bank.setIndex(-1);
     Account account;


     while((choice=menu_list())!= 0)
     	{
    	 switch( choice )

		{

		case 1:

			account.accept_account_info();

			accNumber = bank.create_account(account);
			account.print_account_number(accNumber);
			break;
		case 2:
			account.accept_account_number(accNumber);
			account.accept_amount( amount );
			balance = bank.deposit(accNumber, amount );
			account.print_balance(balance);
			break;
		case 3:
			account.accept_account_number(accNumber);
			account.accept_amount(amount );
			balance = bank.withdraw(accNumber, amount );
			account.print_balance(balance);
			break;
		case 4:
			account.accept_account_number(accNumber);
			account = bank.get_account_details(accNumber);
			account.print_account_info();
			break;
		}
	}
	return 0;
}
