/*
 * main.cpp
 *
 *  Created on: 05-Aug-2020
 *      Author: sunbeam
 */

#include<iostream>
#include<vector>
#include<string>
using namespace std;
int cnt = 1000;
int sno=-1;
class transactions
{
private:
	int accno;
	string date;
	string type;
	float amount;

public:

	transactions()
{
		this->accno=accno;
		this->date=date;
		this->type=type;
		this->amount=amount;
}

	void trans(int accno,float amount,string type)
	{
		this->accno=accno;
		this->amount=amount;
		this->date="05-Aug-2020";
		this->type=type;
	}
	int getaccno()
	{
		return this->accno;
	}
	string getdate()
	{return this->date;

	}
	string gettype()
	{
		return this->type;
	}
	float getamount()
	{
		return this->amount;
	}



};
class account
{
private:
	string name;
	string type;
	int accno;
	float bal;
public:

	account()
{
		this->name=name;
		this->type=type;
		this->accno=accno;
		this->bal=bal;


}
	void setaccno(int num)
	{
		this->accno = num;
	}
	void setname(string str)
	{
		this->name = str;
	}
	void settype(string str)
	{
		this->type = str;
	}
	void setbal(float str)
	{
		this->bal = str;
	}
	int getaccno()
	{
		return this->accno;
	}
	string getname()
	{
		return this->name;
	}
	string gettype()
	{
		return this->type;
	}
	float getbalance()
	{
		return this->bal;
	}


	~account(void)
     {
		cout<<"~Destructor is call "<<endl;
     }

};

class bank
{
private:
	int index;
	vector<transactions> t;
	account*v=new account[5];
public:
	bank()
{
		this->index = -1;
		t.resize(50);
}
	void createacc()
	{
		index++;
		string nme, type;
		float balance;
		v[index].setaccno(++cnt);

		cout << "Enter Name: "<<endl;
		cin >> nme;
		cout << "Enter Type: "<<endl;
		cin >> type;
		cout << "Enter Balance: "<<endl;
		cin >> balance;
		v[index].setname(nme);
		v[index].settype(type);
		v[index].setbal(balance);
		cout<<"Account no is:  "<<v[index].getaccno()<<endl;
		v[index].getaccno();
	}
	void printrec()
	{
		int data;
		cout << "Enter Your Acc No: ";
		cin >> data;
		if(data<1000 || data>cnt)
			throw "Enter Valid Acc No ";
		for (int i = 0; i <= index; i++)
		{
			if (data == v[i].getaccno())
			{
				cout << "Account number	    :" << v[i].getaccno() << endl;
				cout << "Name				:" << v[i].getname() << endl;
				cout << "Type				:" <<v[i].gettype() << endl;
				cout << "Balance			:" << v[i].getbalance() << endl;
				return;
			}
		}

	}
	void deposite()
	{

		int num;
		float data,updated;
		cout << "Enter AccNo To Deposite: ";
		cin >> num;
			for (int i = 0; i <= index; i++)
			{
				if (num == v[i].getaccno())
				{
					cout << "Enter Amount To Deposite: " << endl;
					cin >> data;
					cout << "Account number	:" << v[i].getaccno() << endl;
					cout << "Name				:" << v[i].getname() << endl;
					cout << "Type				:" << v[i].gettype() << endl;
					cout << "Updated Balance: " << (v[i].getbalance() + data) << endl;
					updated=(v[i].getbalance()) + data;
					v[i].setbal(updated);
					sno++;
					t[sno].trans(num,data,"deposite");
					return;
				}
			}
			throw "Enter Valid Acc No ";
	}
	void withdraw()
	{
		int  num;
		float data,updated;
		cout << "Enter AccNo To Withdraw: " << endl;
		cin >> num;


			for (int i = 0; i <= index; i++)
			{
				if (num == v[i].getaccno())
				{
					cout << "Enter Amount To Withdraw: ";
					cin >> data;
					try
					{
						if (data > v[i].getbalance())
						throw "Insufficient Balance ";
					}
					catch (char const* messege)
					{
						cout << messege;
						return;
					}
					cout << "Account number	:" << v[i].getaccno() << endl;
					cout << "Name				:" << v[i].getname() << endl;
					cout << "Type				:" << v[i].gettype() << endl;
					cout << "Updated Balance: " << (v[i].getbalance() - data) << endl;
					updated=(v[i].getbalance()) - data;
					v[i].setbal(updated);
					sno++;
					t[sno].trans(num,data,"Withdraw");
					return;
				}
			}
			throw "Enter Correct Acc No: ";
	}
	void transdata()
	{

		int data;
				cout << "Enter Your Acc No: ";
				cin >> data;
				if(data<1000 || data>cnt)
					throw "Enter Valid Acc No ";
				for (int i = 0; i <= sno; i++)
				{
					if (data == t[i].getaccno())
					{
						cout << "Account number		:"<<"Date		:"<<"Type		:"<<"Amount		:"<<endl;
						cout<< t[i].getaccno()<<"			"<<t[i].getdate()<<"	"<<t[i].gettype()<<"	 "<<t[i].getamount()<< endl;
					}
				}
	}
	void deletememory()
	{
      delete v;
      v =NULL;
	}
};



int main()
{
	int choice;
	bank bk;

	do
	{
		try
		{
			cout << "\n0.Exit"<<endl;;
			cout<<"1.Create"<<endl;
			cout<<"2.PrintDetails" <<endl;
			cout<<"3.Deposite"<<endl;
			cout<<"4.Withdraw "<<endl;
			cout<<"5.Transactions" <<endl;
			cout<<"Enter your choice::  ";
			cin >> choice;
			switch (choice)
			{
			case 0:
				break;
			case 1:bk.createacc();
			break;
			case 2:bk.printrec();
			break;
			case 3:bk.deposite();
			break;
			case 4:bk.withdraw();
			break;
			case 5:bk.transdata();
			break;
			}
		}

		catch (char const* messege)
		{
			cout << messege;
		}

	} while (choice != 0);

	bk.deletememory();

	return 0;
}
