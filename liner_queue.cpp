/*
 * main.cpp
 *
 *  Created on: 04-Aug-2020
 *      Author: sunbeam
 */

#include<iostream>
using namespace std;
#define max_size 5
class queue
{
private:
	int queue[max_size];
	int front=-1;
	int rear=-1;
public:
	void setfront(int front) {
		this->front = -1;
	}

	int getfront() {
		return this->front;
	}
	void setrear(int front) {
		this->rear = -1;
	}

	int getrear() {
		return this->rear;
	}
	int isFull()
	{
		int full = 0 ;

		if( rear == max_size-1 )
			full = 1;

		return full;
	}

	//To check queue is empty or not
	int isEmpty()
	{
		int empty = 0 ;

		if( front == rear + 1 )
			empty = 1;

		return empty;
	}
	void  enqueue()
	{
		int item;
		if(rear==(max_size-1))
		{
			printf("\nQueue Overflow:");
		}
		else
		{
			cout<<"Enter the element to be inserted:\t";
			cin>>item;
			rear=rear+1;
			queue[rear]=item;

			if(front==-1)
				front=0;
		}

	}
	void  dequeue(void)
	{
		int item;
		if(front==-1)
		{
			cout<<"\nQueue Underflow:";
		}
		else
		{
			item=queue[front];
			cout<<"\nThe deleted element: \t"<<item<<endl;
			if(front==rear)
			{
				front=-1;
				rear=-1;
			}
			else
			{
				front=front+1;
			}
		}
	}
	void display()
	{
		int i;
		if(front==-1)
		{
			cout<<"\nQueue is Empty:";
		}
		else
		{
			cout<<"\nThe queue elements are:\n" ;
			for(i=front;i<=rear;i++)
			{
				cout<<queue[i];
			}
		}

	}

};

int main()
{
	queue q1;
	int choice;
	do{

		cout<<"\n\n--------QUEUE OPERATIONS-----------\n";
		cout<<"1. Enqueue\n";
		cout<<"2. dequeue\n";
		cout<<"3. IsFull\n";
		cout<<"4. IsEmpty\n";

		cout<<"5.Display\n";
		cout<<"6.Exit\n";
		cout<<"-----------------------";
		cout<<"\nEnter your choice:\t";
		cin>>choice;
		switch(choice)
		{
		case 1:
			q1.enqueue();
			break;
		case 2:
			q1. dequeue();
			break;
		case 3:
			if(q1.isFull())
			{
				cout<<"Queue is full"<<endl;
			}
			else{
				cout<<"Queue is not full"<<endl;
			}
			break;
		case 4:
			if(q1.isEmpty())
			{
				cout<<"queue is empty"<<endl;
			}
			else{
				cout<<"queue is not empty"<<endl;
			}
			break;
		case 5:
			q1.display();
			break;
		case 6:
			exit(0);
			break;
		default:
			cout<<"\nInvalid choice:\n";
			break;
		}
	}while(choice!=6);
	return 0;
}
