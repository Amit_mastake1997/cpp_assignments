/*
 * main.cpp
 *
 *  Created on: 05-Aug-2020
 *      Author: sunbeam
 */


#include <bits/stdc++.h>
using namespace std;

class Stack
{
private:
	int a[5];
	int top;

public:

	Stack()
  {
		top =-1;
		for(int i=0;i<5;i++)
		{
			this->a[i]=0;
		}
  }

	void  push(int x)
	{
		if (isFull())
		{
			cout<<"stack overflow"<<endl;
		}
		else
		{
			top++;
			a[top]=x;
		}
	}

	void pop()
	{
		if(isEmpty())
		{
			cout<<"\n Sack is underflow  ";
		}
		else
		{
			cout<<"\nDeleted item is : "<<a[top]<<"\n";
			top--;
		}

	}

	bool isEmpty()
	{
		if (top==-1)
			return true;
		else
			return false;
	}

	bool  isFull(){
		if (top==4)
			return true;
		else
			return false;
	}


	int  peek(int pos){
		if (isEmpty())
		{
			cout<<"stack underflow "<<endl;
			return 0;
		}
		else
		{
			return a[pos];
		}
	}


	void display()
	{

		if(isEmpty())
		{
			cout<<"\n Stack is empty";
		}
		else
		{
			cout<<"All values in the stack are "<<endl;
			for(int i= top;i>=0;i--){
				cout<<this->a[i]<<endl;
			}

		}
	}

	~Stack(void)
	  {
		cout<<"Stack Destructed successfully\n";
	  }



};



int main()
{
	Stack s1;
	int choice,x,position;

	do
	{
		cout<<endl;
		cout<<"0 - Exit."<<endl;
		cout<<"1 - Push Item."<<endl;
		cout<<"2 - Pop Item."<<endl;
		cout<<"3 - isEmpty."<<endl;
		cout<<"4 - isFull."<<endl;
		cout<<"5 - Peek."<<endl;
		cout<<"6 - Display."<<endl;

		cout<<"Enter your choice: ";
		cin>>choice;

		switch(choice){

		case 0:

			break;
		case 1:
			for(int i=0;i<=5;i++){
				cout<<"Enter an iteam to push in the stack"<<endl;
				cin>>x;
				s1.push(x);
			}
			break;
		case 2:
		     s1.pop();
			break;
		case 3:
			if(s1.isEmpty())
			{
				cout<<"stack is empty"<<endl;
			}
			else{
				cout<<"stack is not empty"<<endl;
			}
			break;

		case 4:
			if(s1.isFull())
			{
				cout<<"stack is full"<<endl;
			}
			else{
				cout<<"stack is not full"<<endl;
			}
			break;

		case 5:
			cout<<"Enter position of iteam want to peek : "<<endl;
			cin>>position;
			cout<<"Peek function called: "<<position<<" is "<<s1.peek(position)<<endl;
			break;

		case 6:
			s1.display();
			break;
		}
	}while(choice!=0);


	return 0;
}

