/*
 * main.cpp
 *
 *  Created on: 10-Aug-2020
 *      Author: sunbeam
 */

#include<iostream>
#include<string>
using namespace std;

class Exception
{
private:
	string message;
public:
	Exception( string message ) throw( ) : message( message )
{	}
	string getMessage( void )const throw( )
							{
		return this->message;
							}
};

class LinkedList
{
private:
	class Node
	{
	private:
		int data;
		Node *next;
	public:
		Node( int data = 0 )throw( )
		{
			this->data = data;
			this->next = NULL;
		}
		friend class LinkedList;
	};
private:
	Node *head;
	Node *tail;
public:
	LinkedList( void )throw( )
	{
		this->head = NULL;
		this->tail = NULL;
	}
	bool empty( void )const throw( )
							{
		return this->head == NULL;
							}

	void createnode()
	{
		int value;
		Node *temp=new Node;
		temp->data=value;
		temp->next=NULL;
		if(head==NULL)
		{
			head=temp;
			tail=temp;
			temp=NULL;
		}
		else
		{
			tail->next=temp;
			tail=temp;
		}
	}

	void addFirst(void)throw(bad_alloc)
							  {
		int value;
		cout<<"Enter the value to be inserted: ";
		cin>>value;
		Node *newNode=new Node;
		newNode=new Node(value);
		if( this->empty( ) )
			this->head == newNode;
		else
			newNode->data=value;
		newNode->next=head;
		head=newNode;
		cout<<"Element Inserted at beginning"<<endl;
							  }

	void addLast( void )throw( bad_alloc )
	{
		int value;

		cout<<"Enter the value to be inserted: ";
		cin>>value;
		Node *newNode = new Node( value );
		if(head==NULL)
			head=newNode;
		else{
			Node *trav = this->head;
			while(trav->next!=NULL)
				trav=trav->next;
			trav->next=newNode;

		}
		cout<<"Element Inserted at last"<<endl;
			}

	void insert_position(void)throw( bad_alloc )
	 {
		int pos;
		int value;
		cout<<"Enter the value to be inserted: ";
		cin>>value;
		cout<<"Enter the postion at which node to be inserted: ";
		cin>>pos;
		Node *pre=new Node;
		    Node *cur=new Node;
		    Node *temp=new Node;
		    cur=head;
		    for(int i=1;i<pos;i++)
		    {
		      pre=cur;
		      cur=cur->next;
		    }
		    temp->data=value;
		    pre->next=temp;
		    temp->next=cur;

	  }

	void removeFirst( )throw(Exception)
							{
		if( this->empty())
			throw Exception("LinkedList is empty");
		else if( this->head == this->tail)
		{
			delete this->head;
			this->head = this->tail = NULL;
		}
		else
		{
			Node *ptrNode = this->head;
			this->head = this->head->next;
			delete ptrNode;
		}
		}

	void delete_last( )throw(Exception)
		{
		if( this->empty())
			throw Exception("LinkedList is empty");
		else if( this->tail == this->head)
		{
			delete this->tail;
			this->tail = this->head = NULL;
		}
		else
		{
			Node *current=new Node;
			Node *previous=new Node;
			current=head;
			while(current->next!=NULL)
			{
				previous=current;
				current=current->next;
			}
			tail=previous;
			previous->next=NULL;
			delete current;
		}
							}

	void delete_position()throw(Exception)
	{
		int pos;
		cout<<"Enter the position of value to be deleted: ";
	    cin>>pos;
		Node *current=new Node;
		Node *previous=new Node;
		current=head;
		for(int i=1;i<pos;i++)
		{
			previous=current;
			current=current->next;
		}
		previous->next=current->next;
	}

	 void reverse()throw(Exception)
	    {

		  Node *prevNode, *curNode;

		     if(head != NULL)
		     {
		         prevNode = head;
		         curNode = head->next;
		         head = head->next;

		         prevNode->next = NULL; // Make first node as last node

		         while(head != NULL)
		         {
		             head = head->next;
		             curNode->next = prevNode;

		             prevNode = curNode;
		             curNode = head;
		         }

		         head = prevNode;
		     }
	    }

	void print( void )const throw( Exception )
							{
		if( this->empty() )
			throw Exception("LinkedList is empty");
		Node *trav = this->head;
		while( trav != NULL )
		{
			cout<<trav->data<<"	";
			trav = trav->next;
		}
		cout<<endl;
							}
	~LinkedList( void )throw( )
							{
		while( !this->empty( ) )
			this->removeFirst();
							}
};

int main( void )
{
	try
	{
		LinkedList list;
		int choice;
		do{
			cout<<"\n0.Exit"<<endl;
			cout<<"1.insert first "<<endl;
			cout<<"2.insert last  "<<endl;
			cout<<"3.insert position   "<<endl;
			cout<<"4.print    "<<endl;
			cout<<"5.deleteFirst  "<<endl;
			cout<<"6.delete last   "<<endl;
			cout<<"7.delete position   "<<endl;
			cout<<"8.reverse the list   "<<endl;
			cout<<"Enter the choice: ";
			cin>>choice;
			switch(choice)
			{
			case 0:
				break;

			case 1:
				list.addFirst();
				break;
			case 2:
				list.addLast();
				break;
			case 3:
				list.insert_position();
				break;
			case 4:
				list.print();
				break;
			case 5:
				list.removeFirst();
				break;
			case 6:
				list.delete_last();
				break;
			case 7:
				list.delete_position();
				break;
			case 8:
				list.reverse();
				list.print();
				break;

			}
		}while (choice != 0);

	}
	catch( Exception &ex )
	{
		cout<<ex.getMessage()<<endl;
	}
	return 0;
}



