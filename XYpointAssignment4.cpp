/*
 * main.cpp
 *
 *  Created on: 30-Jul-2020
 *      Author: sunbeam
 */

#include<iostream>
#include<cstring>
using namespace std;
int x, y;
class Point {
private:
	mutable int x;
	mutable int y;
public:
	Point() {
		this->x = 0;
		this->y = 0;
	}
	Point(const int x, const int y) {
		this->x = x;
		this->y = y;
	}
	int getXposition() const {
		return this->x;
	}
	void setXposition(const int xposition) {
		this->x = xposition;
	}
	int getYposition() const {
		return this->y;
	}
	void setYposition(const int yposition) {
		this->y = yposition;
	}
	void modifyvalue() const {
		++this->x;
		++this->y;
	}
};
void Accept_record(Point *ptr) {
	int xelement, yelement;
	cout << "Enter the x element:" << endl;
	cin >> xelement;
	ptr->setXposition(xelement);
	cout << "Enter the y element:" << endl;
	cin >> yelement;
	ptr->setYposition(yelement);
}
void Display_record(Point *ptr) {
	cout << "X value : " << ptr->getXposition() << endl;
	cout << "Y value : " << ptr->getYposition() << endl;
}
int Exit()
{
	cout<<"Bye.....Have Good Day..."<<endl;
	return 0;
}
int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Accept_Record"<<endl;
	cout<<"2.Display_Record"<<endl;
	cout<<"3.Modify"<<endl;
	cout<<"Enter choice	:	"<<endl;
	cin>>choice;
	return choice;
}
int main( void )
{
	int choice;
	Point p;
     while((choice=menu_list())!= 0)
     	{
    	 switch( choice )

		{
    	 case 0:
    		 Exit();
    		 break;
		case 1:
			Accept_record(&p);
			break;
		case 2:
			Display_record(&p);
			break;
		case 3:
			  p.modifyvalue();
			  Display_record(&p);
			break;
		}
	}
	return 0;
}

