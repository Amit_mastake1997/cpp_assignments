
/*
 * Main.cpp
 *
 *  Created on: 03-Aug-2020
 *      Author: sunbeam
 */

#include<iostream>
#include<string>
using namespace std;
int count = 1000;

class IllegalArgumentException
{
private:
	string message;
public:
	IllegalArgumentException( const string message ) throw( ) : message( message )
{	}
	string getMessage( void )const throw( )
							{
		return this->message;
							}
};

class Account
{
private:
	string name;
	int number;
	string type;
	float balance;
public:

	Account( void ) throw( ) : number( 0 ), balance( 0 )
{	}

	Account( int number, float balance )
	{
		this->setNumber(number);
		this->setBalance(balance);
	}

	void setBalance(float balance)throw(IllegalArgumentException)
			{
		if(balance<0)
			throw IllegalArgumentException("Please Enter Proper Bank Balance");
		this->balance = balance;
			}

	float getBalance(void) const throw()
					{
		return this->balance;
					}


	void setNumber(const int number)throw(IllegalArgumentException)
			{
		if(number<0)
			throw IllegalArgumentException(" Account number is not found");
		this->number = number;
			}


	int getNumber(void) const throw()
					{
		return this->number;
					}


	void acceptAccountInfo( )throw(IllegalArgumentException)
					{
		cout<<"Name	:	";
		cin>>this->name ;
		cout<<"Type	:	";
		cin>>this->type;
		cout<<"Enter  balance	:	";
		cin>>balance;
		setBalance(balance);

					}

	void printAccountInfo()throw()
					{

		cout<<"Name :"<<this->name<<endl;
		cout<<"number:"<<this->getNumber()<<endl;
		cout<<"type:"<<this->type<<endl;
		cout<<"balance:"<<this->getBalance()<<endl;
					}

	void acceptAccountNumber( int &number )throw(IllegalArgumentException)
				{
		setNumber(number);
		cout<<"Account number	:	"<<endl;
		cin>>number ;

				}

	void acceptAmount( float &amount )throw(IllegalArgumentException)
					{
		if( amount < 0 )
			throw IllegalArgumentException("Invalid amount");

		cout<<"Amount	:	"<<endl;
		cin>>amount ;
					}

	void printAccountNumber(int &number )throw()
					{
		cout<<"Account number	:"<<number<<endl;
					}

	void printBalance( float &balance )throw()
					{
		cout<<"Balance	:"<<balance<<endl ;
					}


	string getName() const
	{
		return name;
	}


	string getType() const
	{
		return type;
	}
};

class Bank
{
private:
	int index;
	Account arr[ 5 ];
public:

	int createAccount(Account &account)throw()
	{
		account.setNumber(++ count);

		this->arr[ ++ this->index] = account;
		return account.getNumber();
	}

	float deposit( int number, float amount )throw(IllegalArgumentException)
					{
		if(number<0)
			throw IllegalArgumentException(" Account number is not found");
		for( int index = 0; index <= this->index; ++ index )
		{
			if(this->arr[ index ].getNumber()== number  )
			{
				float bal=this->arr[ index ].getBalance();
				bal+= amount;
				this->arr[ index ].setBalance(bal);
				return this->arr[ index ].getBalance();
			}
		}
		return 0;
					}

	float withdraw(  int number, float amount )throw(IllegalArgumentException)
					{
		if(number<0)
			throw IllegalArgumentException(" Account number is not found");
		for( int index = 0; index <= this->index; ++ index )
		{
			if( this->arr[ index ].getNumber() == number  )
			{
				float bal=this->arr[ index ].getBalance();
				bal-= amount;
				this->arr[ index ].setBalance(bal);
				return this->arr[ index ].getBalance();
			}
		}
		return 0;
					}

	class Account getAccountDetails(int number )throw(IllegalArgumentException)
			{
		if(number<0)
			throw IllegalArgumentException(" Account number is not found");
		class Account account = { };
		for( int index = 0; index <= this->index; ++ index )
		{
			if( this->arr[ index ].getNumber() == number  )
			{
				account = this->arr[ index ];
			}
		}
		return account;
			}

	int getIndex() const
	{
		return index;
	}

	void setIndex(int index)
	{
		this->index = index;
	}
};


int menu_list( void )
{
	int choice;
	cout<<"\n0.Exit\n";
	cout<<"1.Create New Account\n";
	cout<<"2.Deposit\n";
	cout<<"3.Withdraw\n";
	cout<<"4.Print Account details\n";
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}

int main( void )
{
	try{
		int choice;
		int accNumber;
		float balance, amount;
		Bank bank ;
		bank.setIndex(-1);
		Account account;


		while((choice=menu_list())!= 0)
		{
			try{

				switch( choice )
				{
				case 1:
					account.acceptAccountInfo();
					accNumber = bank.createAccount(account);
					account.printAccountNumber(accNumber);
					break;
				case 2:
					account.acceptAccountNumber(accNumber);
					account.acceptAmount( amount );
					balance = bank.deposit(accNumber, amount );
					account.printBalance(balance);
					break;
				case 3:
					account.acceptAccountNumber(accNumber);
					account.acceptAmount(amount );
					balance = bank.withdraw(accNumber, amount );
					account.printBalance(balance);
					break;
				case 4:
					account.acceptAccountNumber(accNumber);
					account = bank.getAccountDetails(accNumber);
					account.printAccountInfo();
					break;
				}

			}
			catch( IllegalArgumentException &ex )
			{
				cout<<ex.getMessage()<<endl;
			}
		}
	}
	catch(...)
	{
		cout<<"Exception"<<endl;
	}
	return 0;

}

