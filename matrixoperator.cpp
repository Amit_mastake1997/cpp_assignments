/*
 * main.cpp
 *
 *  Created on: 11-Aug-2020
 *      Author: sunbeam
 */

#include<iostream>
using namespace std;
#define size	3
class Matrix
{
	int row;
	int col;
	int **arr;
public:
	Matrix();
	void accept();
	void display();
	void operator +(Matrix x);
	void operator -(Matrix x);
	void operator *(Matrix x);
	~Matrix();
};

Matrix::Matrix()
{
	this->row=0;
	this->col=0;
	arr=NULL;
	arr = new int*[row];
	for(int row=0;row<size;row++)
		arr[row]=new int[col];
}

void Matrix::accept()
{
	cout<<"\n Enter Matrix Element (3 X 3) : \n";
	for(int row=0;row<size;row++)
		for(int col=0; col<size; col++)
		{
			cout<<"arr[ "<<row<<" ][ "<<col<<" ]	:	";
			cin>>arr[row][col];
		}
	cout<<endl;
}
void Matrix::display()
{
	for( int row = 0; row < size; ++ row )
	{
		for( int col = 0; col < size; ++ col )
		{
			cout<<arr[ row ][ col ]<<"	";
		}
		cout<<endl;
	}
}
void Matrix::operator +(Matrix x)
{
	int mat[3][3];
	for(int i=0; i<3; i++)
	{
		for(int j=0; j<3; j++)
		{
			mat[i][j]=arr[i][j]+x.arr[i][j];
		}
	}
	cout<<"\n Addition of Matrix : \n\n";
	for(int i=0; i<3; i++)
	{
		cout<<" ";
		for(int j=0; j<3; j++)
		{
			cout<<mat[i][j]<<"\t";
		}
		cout<<"\n";
	}
}

void Matrix::operator -(Matrix x)
{
	int mat[3][3];
	for(int i=0; i<3; i++)
	{
		for(int j=0; j<3; j++)
		{
			mat[i][j]=arr[i][j]-x.arr[i][j];
		}
	}
	cout<<"\n Subtraction of Matrix : \n\n";
	for(int i=0; i<3; i++)
	{
		cout<<" ";
		for(int j=0; j<3; j++)
		{
			cout<<mat[i][j]<<"\t";
		}
		cout<<"\n";
	}
}

void Matrix::operator *(Matrix x)
{
	int mat[3][3],sum=0;
	for(int i=0; i<3; i++)
	{
		for(int j=0; j<3; j++)
		{
			 for(int k=0; k<3; k++)
			 {
				 sum=sum+arr[i][k]*x.arr[k][i];

			 }
			 mat[i][j]=sum;
			 sum=0;

		}
	}
	cout<<"\n Multiplaction of Matrix : \n\n";
	for(int i=0; i<3; i++)
	{
		cout<<" ";
		for(int j=0; j<3; j++)
		{
			cout<<mat[i][j]<<"\t";
		}
		cout<<"\n";
	}
}
Matrix::~Matrix()
	{
		for(int row=0;row<3;row++)
			delete arr[row];
		delete arr;
	}

int main()
{
	Matrix m,n;

	int choice;
	Matrix m1;

	do{
		cout<<"0.Exit"<<endl;
		cout<<"1.Accept "<<endl;
		cout<<"2.print "<<endl;
		cout<<"3.Matrix Addition            "<<endl;
		cout<<"4.Matrix Subtraction         "<<endl;
		cout<<"5.Matrix Multiplication      "<<endl;
		cout<<"Enter the choice: ";
		cin>>choice;
		switch(choice)
		{
		case 0:
			break;
		case 1:
			m1.accept();
			break;
		case 2:
			m1.display();
			break;
		case 3:
			m.accept();n.accept();
			cout<<"\n First Matrix : \n\n";
			m.display();
			cout<<"\n Second Matrix : \n\n";
			n.display();
			m+n;
			break;
		case 4:
			m.accept();n.accept();
			cout<<"\n First Matrix : \n\n";
			m.display();
			cout<<"\n Second Matrix : \n\n";
			n.display();
			m-n;
			break;
		case 5:
			m.accept(); n.accept();
			cout<<"\n First Matrix : \n\n";
			m.display();
			cout<<"\n Second Matrix : \n\n";
			n.display();
			m*n;
			break;
		}
	}while (choice != 0);

	return 0;
}
