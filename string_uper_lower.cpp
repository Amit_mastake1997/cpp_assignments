/*
 * main.cpp
 *
 *  Created on: 07-Aug-2020
 *      Author: sunbeam
 */

#include<cstring>
#include<iostream>
using namespace std;
class String
{
private:
	size_t length;
	char *buffer;
public:
	String( void ) : length ( 0 ), buffer( NULL )
	{	}
	String( size_t length ) : length ( length ), buffer( new char[ this->length ]  )
	{	}
	String( const char *str )
	{
		this->length = strlen(str);
		this->buffer = new char[ this->length + 1 ];
		strcpy( this->buffer, str );
	}
	String( const String &other )
	{
		this->length = other.length;
		this->buffer = new char[ this->length + 1 ];
		strcpy( this->buffer, other.buffer );
	}
	void printRecord( void )
	{
		cout<<this->buffer<<endl;
	}
	//toUpperCase
	void toUpperCase( )
	{
      for(int i=0;i<sizeof(this->buffer);++i)
      {
    	  if(buffer[i]>=97 && buffer[i]<=122)
    	  {
    		  buffer[i]=buffer[i]-32;
    	  }
      }
	}
	//toLowerCase
	void toLowerCase( )
	{
		for(int i=0;i<sizeof(this->buffer);++i)
		      {
		    	  if(buffer[i]>=65 && buffer[i]<=92)
		    	  {
		    		  buffer[i]=buffer[i]+32;
		    	  }
		      }
	}
	~String( void )
	{
		if( this->buffer != NULL )
		{
			delete[] this->buffer;
			this->buffer = NULL;
		}
	}
};
int main( void )
{
	String s1("Welcome to Karad Sunbeam");
	String s2 = s1;
	s2.printRecord( );
	s2.toUpperCase();
	s2.printRecord( );
	s2.toLowerCase();
    s2.printRecord( );
	return 0;

}

