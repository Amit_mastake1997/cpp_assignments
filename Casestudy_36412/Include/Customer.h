/*
 * Customer.h
 *
 *  Created on: 20-Aug-2020
 *      Author: sunbeam
 */

#ifndef CUSTOMER_H_
#define CUSTOMER_H_
#include"../Include/Vehicle.h"

using namespace std;
namespace kd3
{
class Customer
{
	string name;
	string address;
	string mobile;
public:
	vector<Vehicle> vehList;

	Customer(string name = " ",string address = " ",string mobile = " ");
	string getAddress() const;
	void setAddress(const string &address);
	string getMobile() const;
	void setMobile(const string &mobile);
	string getName() const;
	void setName(const string &name);
	const vector<Vehicle>& getVehList() const;
	void setVehList(const vector<Vehicle> &vehList);
	void acceptRecord();
	Vehicle& newVehicle();
	void writeInFile( Customer &c);
	void printVehicle();
	void printRecord();
	Vehicle& getSingleVehicle(int i);
	static void readFromFile(vector<Customer> &c);
	friend ostream& operator<<(ostream &cout,Customer &other);
	friend istream& operator>>(istream &cin,Customer &other);
	friend class ServiceStation;
};
}


#endif /* CUSTOMER_H_ */
