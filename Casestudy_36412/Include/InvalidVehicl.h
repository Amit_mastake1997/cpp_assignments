/*
 * InvalidVehicle.h
 *
 *  Created on: 20-Aug-2020
 *      Author: sunbeam
 */

#ifndef INVALIDVEHICLE_H_
#define INVALIDVEHICLE_H_

#include<string>
using namespace std;
namespace kd3
{
class InvalidVehicel
{
	string message;
public:
	InvalidVehicel(string mes);
	string getMessage();
};
}



#endif /* INVALIDVEHICLE_H_ */
