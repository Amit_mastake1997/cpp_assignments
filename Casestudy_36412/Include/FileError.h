/*
 * FileError.h
 *
 *  Created on: 20-Aug-2020
 *      Author: sunbeam
 */

#ifndef FILEERROR_H_
#define FILEERROR_H_


#include<string>
using namespace std;
namespace kd3
{
class FileHandlingError
{
	string message;
public:
	FileHandlingError(string mes);
	string getMessage();
};
}



#endif /* FILEERROR_H_ */
