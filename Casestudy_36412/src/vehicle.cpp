/*
 * vehicle.cpp
 *
 *  Created on: 20-Aug-2020
 *      Author: sunbeam
 */
#include"../Include/Vehicle.h"


namespace kd3
{
ostream& operator<<(ostream &cout,Vehicle &other)
	{
		cout<<"Company :		"<<other.company<<endl;
		cout<<"Model  :		"<<other.model<<endl;
		cout<<"Number :		"<<other.number<<endl;
		return cout;
	}
	istream& operator>>(istream &cin,Vehicle &other)
	{
		string m;
		cout<<"Enter Vehicel Company :	";
		getline(cin,m);
		other.setCompany(m);
		cout<<"Enter Vehicel Model :	";
		getline(cin,m);
		other.setModel(m);
		cout<<"Enter Vehicel Number :	";
		getline(cin,m);
		other.setNumber(m);
		return cin;
	}
}

using namespace kd3;
Vehicle::Vehicle(string company,string model,string number,string mobileNum):company(company),model(model),number(number),mobileNum(mobileNum)
{}
	Vehicle::Vehicle(const Vehicle &other)
	{
		this->company = other.company;
		this->model = other.model;
		this->number = other.number;
		this->mobileNum = other.mobileNum;
	}
	Vehicle& Vehicle::operator=(const Vehicle &other)
	{
		this->company = other.company;
		this->model = other.model;
		this->number = other.number;
		return *this;
	}

	void Vehicle::acceptRecord(string mobileNum )
	{
		string m;
		cout<<"Enter Vehicel Company :	";
		getline(cin,m);
		this->setCompany(m);
		cout<<"Enter Vehicel Model :	";
		getline(cin,m);
		this->setModel(m);
		cout<<"Enter Vehicel Number :	";
		getline(cin,m);
		this->setNumber(m);
		this->mobileNum = mobileNum;
	}
	void Vehicle::setMobileNum(string &mobileNum)
	{
		this->mobileNum = mobileNum;
	}

	void Vehicle::printRecord()
	{
		cout<<"Company :		"<<this->company<<endl;
		cout<<"Model  :		"<<this->model<<endl;
		cout<<"Number :		"<<this->number<<endl;

	}
	string Vehicle::getCompany() const
	{
		return company;
	}
	string Vehicle::getMobileNum()
	{
		return this->mobileNum;
	}

	void Vehicle::setCompany(const string &company)
	{
		if(company==" ")
			throw InvalidVehicel ("Company cant be empty");
		this->company = company;
	}
	void Vehicle::storeRecord()
	{
		ofstream file;
		file.open("vehicles.csv",ios::out | ios::app);

		if(!file)
		{
			throw FileHandlingError("Error in file opening") ;
		}
		else
		{
			file<<this->getCompany()<<","<<this->getModel()<<","<<this->getNumber()<<","<<this->getMobileNum()<<"\n";
		}
	}
	 vector<Vehicle>& Vehicle::readRecord()
	{
		ifstream file;
		static vector<Vehicle> m;
		file.open("vehicles.csv",ios::out | ios::app);

		if(!file)
		{
			throw FileHandlingError("Error in file opening") ;
		}
		else
		{
			string line;

			while(getline(file,line))
			{
				stringstream str(line);
				string token[4];
				for(int i = 0;i<4;i++)
				{
					getline(str,token[i],',');
				}
				Vehicle v(token[0],token[1],token[2],token[3]);
				m.push_back(v);
			}

		}
		return m;
	}
	string Vehicle::getModel() const
	{
		return model;
	}

	void Vehicle::setModel(const string &model)
	{
		if(model==" ")
			throw InvalidVehicel ("Company cant be empty");
		this->model = model;
	}

	string Vehicle::getNumber() const
	{
		return number;
	}

	void Vehicle::setNumber(const string &number)
	{
		if(company==" ")
			throw InvalidVehicel ("Company cant be empty");
		this->number = number;
	}
