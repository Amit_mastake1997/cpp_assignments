/*
 * part.cpp
 *
 *  Created on: 20-Aug-2020
 *      Author: sunbeam
 */

#include"../Include/Part.h"
#include<fstream>
#include<sstream>
#include"../Include/FileError.h"
using namespace std;
using namespace kd3;
Part::Part(string desc,double rate):desc (desc),rate(rate)
{}
void Part::acceptRecord()
{
	cout<<"Enter part Details:	";
	cin>>desc;
	cout<<"Enter Rate:	";
	cin>>this->rate;
	this->storePartRecord();
	cout<<"New Part added into record"<<endl;
}
void Part::storePartRecord()
{
	ofstream out;
	out.open("Part.csv",ios::out|ios::app);
	if(!out)
		throw FileHandlingError("Error in file opening");
	else
	{
		out<<this->desc<<","<<this->rate<<"\n";
	}
}
void Part::fetchRecord(list<Part> &partList)
{
	ifstream in;
	in.open("Part.csv");
	if(!in)
		throw FileHandlingError("Error in file opening");
	else
	{
		string line;
		while(getline(in,line))
		{
			stringstream str(line);
			string token[2];
			for(int i = 0;i<2;i++)
			{
				getline(str,token[i],',');
			}
			Part p1(token[0],stod(token[1]));
			partList.push_back(p1);
		}
		cout<<"Parts record loaded"<<endl;
	}
}
namespace kd3{

ostream& operator<<(ostream& cout,Part &o)
	{
		cout<<"Description 	:		"<<o.desc<<endl;
		cout<<"Rate:	"<<o.rate<<endl;
		return cout;
	}
iostream& operator>>(iostream& cin,Part &other)
{
	cout<<"Enter Descripition:	";
	getline(cin,other.desc);
	cout<<"Enter Rate:	";
	cin>>other.rate;
	return cin;
}

}

void Part::print()
{
	cout<<"Part Details	:		"<<this->desc<<endl;
	cout<<"Rate:		"<<this->rate<<endl;
}
string Part::getDesc() const {
	return desc;
}

void Part::setDesc(const string &desc) {
	this->desc = desc;
}

double Part::getRate() const {
	return rate;
}

void Part::setRate(double rate) {
	this->rate = rate;
}

