/*
 * main.cpp
 *
 *  Created on: 06-Aug-2020
 *      Author: sunbeam
 */

# include<stdio.h>


#include<iostream>
using namespace std;

#define size	3
class matrix
{
	int row;
	int col;
	int **arr;

public:

	matrix()
{
		this->row=0;
		this->col=0;
		arr=NULL;
		arr = new int*[row];
		for(int row=0;row<size;row++)
			arr[row]=new int[col];
}

	void acceptmatrix()
	{
		for(int row=0;row<size;row++)
			for(int col=0; col<size; col++)
			{
				cout<<"arr[ "<<row<<" ][ "<<col<<" ]	:	";
				cin>>arr[row][col];
			}
		cout<<endl;
	}

	void printmatrix()

	{
		for( int row = 0; row < size; ++ row )
		{
			for( int col = 0; col < size; ++ col )
			{
				cout<<arr[ row ][ col ]<<"	";
			}
			cout<<endl;
		}
	}


	void additionTwoMatrix()
	{
		int x[3][3],y[3][3],z[3][3];
		void getmatrix(int [][3]);
		void addition(int [][3],int [][3],int [][3]);

		cout<<"------------------Enter first Matrix Element-------------------- "<<endl;
		matrix::getmatrix(x);
		cout<<"------------------Enter Second Matrix Element-------------------- "<<endl;
		matrix::getmatrix(y);
		matrix::addition(x,y,z);

		cout<<"\n - : Matrix 1: - \n";
		display(x);
		cout<<"\n - : Matrix 2: - \n";
		display(y);
		cout<<"\n - : Matrix Addition (Result): - \n";
		display(z);
	}

	void addition(int p[][3],int q[][3],int r[][3])
	{     int i,j;
	for(i=0;i<3;i++)
	{      for(j=0;j<3;j++)
		r[i][j]=p[i][j]+q[i][j];
	}
	}



	void subtractionTwoMatrix()
	{
		int x[3][3],y[3][3],z[3][3];
		void getmatrix(int [][3]);
		void subtraction(int [][3],int [][3],int [][3]);

		cout<<"------------------Enter first Matrix Element-------------------- "<<endl;
		matrix::getmatrix(x);
		cout<<"------------------Enter Second Matrix Element-------------------- "<<endl;
		matrix::getmatrix(y);

		matrix::subtraction(x,y,z);
		cout<<"\n - : Matrix 1: - \n";
		display(x);
		cout<<"\n - : Matrix 2: - \n";
		display(y);
		cout<<"\n - : Matrix Subtraction (Result): - \n";
		display(z);
	}
	void subtraction(int p[3][3],int q[3][3],int r[3][3])
			{
				int i,j;
				for(i=0;i<3;i++)
				{
					for(j=0;j<3;j++)
						r[i][j]=p[i][j]-q[i][j];
				}
			}

	void multipicationTwoMatrix()
	{
		int x[3][3],y[3][3],z[3][3];
		void getmatrix(int [][3]);
		void getmatrix(int [][3]);

		void multiplication(int [][3],int [][3],int [][3]);

		cout<<"------------------Enter first Matrix Element-------------------- "<<endl;
		matrix::getmatrix(x);
		cout<<"------------------Enter Second Matrix Element-------------------- "<<endl;
		matrix::getmatrix(y);

		matrix::multiplication(x,y,z);
		cout<<"\n - : Matrix 1: - \n";
		display(x);
		cout<<"\n - : Matrix 2: - \n";
		display(y);
		cout<<"\n - : Matrix Multiplication (Result): - \n";
		display(z);
	}

	void multiplication(int p[][3],int q[3][3],int r[3][3])
	{
		int i,j,k;
		for(i=0;i<3;i++)

		{
			for(j=0;j<3;j++)

			{
				r[i][j]=0;
				for(k=0;k<3;k++)
					r[i][j]=r[i][j]+(p[i][j]*q[j][k]);
			}
		}
	}

	void getmatrix(int t[][3])
		{
			int i,j;
			for(i=0;i<3;i++)
			{
				for(j=0;j<3;j++)
				{

					cout<<"Enter Element [ "<<i<<" ][ "<<j<<" ]	:	";

					cin>>t[i][j];
				}
			}
		}

	void display(int m[][3])
	{
		int i,j;
		cout<<"\n\n";
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
				cout<<"  "<<m[i][j];
			cout<<"\n";
		}
	}
	~matrix()
	{
		for(int row=0;row<3;row++)
			delete arr[row];
		delete arr;
	}
};

int main()
{
	int choice;
	matrix m1;

	do{
		cout<<"0.Exit"<<endl;
		cout<<"1.Accept "<<endl;
		cout<<"2.print "<<endl;
		cout<<"3.Matrix Addition            "<<endl;
		cout<<"4.Matrix Subtraction         "<<endl;
		cout<<"5.Matrix Multiplication      "<<endl;
		cout<<"Enter the choice: ";
		cin>>choice;
		switch(choice)
		{
		case 0:
			break;
		case 1:
			m1.acceptmatrix();
			break;
		case 2:
			m1.printmatrix();
			break;
		case 3:
			m1.additionTwoMatrix();
			break;
		case 4:
			m1.subtractionTwoMatrix();
			break;
		case 5:
			m1.multipicationTwoMatrix();
			break;
		}
	}while (choice != 0);
	return 0;
}






