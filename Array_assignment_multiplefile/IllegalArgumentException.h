/*
 * IllegalArgumentException.h
 *
 *  Created on: 07-Aug-2020
 *      Author: sunbeam
 */

#ifndef ILLEGALARGUMENTEXCEPTION_H_
#define ILLEGALARGUMENTEXCEPTION_H_
#include<string>
using namespace std;

class IllegalArgumentException
{
private:
	string message;
public:
	IllegalArgumentException( string message = "Illegal Argument Exception" ) : message ( message )
	{	}
	string getMessage( void )const throw( )
	{
		return this->message;
	}
};




#endif /* ILLEGALARGUMENTEXCEPTION_H_ */
