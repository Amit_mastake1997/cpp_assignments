/*
 * main.cpp
 *
 *  Created on: 07-Aug-2020
 *      Author: sunbeam
 */


#include<iostream>
using namespace std;
#include"Array.h"
using namespace kdac;

void accept_record( Array &array )
{
	int element ;
	for( int index = 0; index < array.getSize(); ++ index )
	{
		cout<<"Enter element	:	";
		cin>>element;
		array.setElement(index,element);
	}
}

void print_record(const Array &array )
{
	int element ;
	for( int index = 0; index < array.getSize(); ++ index )
	{
		element = array.getElement(index);
		cout<<"Element	:	"<<element<<endl;
	}
}

enum IOOperation
{
	EXIT, ACCEPT_RECORD, PRINT_RECORD
};
IOOperation menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Accept Record"<<endl;
	cout<<"2.Print Record"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return IOOperation(choice);
}
int main( void )
{
	try
	{
		IOOperation choice;
		Array array;
		array.setSize(6);
		while( ( choice = ::menu_list( ) ) != EXIT)
		{
			try
			{
				switch( choice )
				{
				case EXIT:
					break;
				case ACCEPT_RECORD:
					accept_record(array);
					break;
				case PRINT_RECORD:
					print_record(array);
					break;
				}
			}
			catch( ArrayIndexOutOfBoundsException &ex )
			{
				cout<<ex.getMessage()<<endl;
			}
		}
	}

	catch( IllegalArgumentException &ex)
	{
		cout<<ex.getMessage()<<endl;
	}

	catch( bad_alloc &ex )
	{
		cout<<ex.what()<<endl;
	}
	catch( ... )
	{
		cout<<"Exception"<<endl;
	}
	return 0;
}
