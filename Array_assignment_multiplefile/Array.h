/*
 * Array.h
 *
 *  Created on: 07-Aug-2020
 *      Author: sunbeam
 */
#include"ArrayIndexOutOfBoundExeption.h"
#include"IllegalArgumentException.h"


#ifndef ARRAY_H_
#define ARRAY_H_
namespace kdac
{
	class Array
	{
	private:
		int size;
		int *arr;
	public:
		Array( void )throw( );
		Array( int size )throw( bad_alloc );
		int getElement( int index )const throw( ArrayIndexOutOfBoundsException);
		void setElement(const int index,const int element)throw(ArrayIndexOutOfBoundsException);
		int getSize( )const throw( );
		void setSize( int size )throw( IllegalArgumentException, bad_alloc );
		~Array( void )throw( );

	};
}
#endif /* ARRAY_H_ */
