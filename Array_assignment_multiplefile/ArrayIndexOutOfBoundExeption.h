/*
 * ArrayIndexOutOfBoundExeption.h
 *
 *  Created on: 07-Aug-2020
 *      Author: sunbeam
 */

#ifndef ARRAYINDEXOUTOFBOUNDEXEPTION_H_
#define ARRAYINDEXOUTOFBOUNDEXEPTION_H_
#include<string>
using namespace std;

namespace kdac{
	class ArrayIndexOutOfBoundsException
	{
	private:
		string message;
	public:
		ArrayIndexOutOfBoundsException( string message = "Array Index Out Of Bounds Exception") : message ( message )
		{	}
		string getMessage( void )const throw( )
		{
			return this->message;
		}
	};
}
#endif /* ARRAYINDEXOUTOFBOUNDEXEPTION_H_ */
