/*
 * Array.cpp
 *
 *  Created on: 07-Aug-2020
 *      Author: sunbeam
 */
#include"Array.h"
#include"IllegalArgumentException.h"
#include"ArrayIndexOutOfBoundExeption.h"
using namespace kdac;

namespace kdac{
Array::Array( void )throw( )
		{
			this->size = 0;
			this->arr = NULL;
		}
		Array::Array( int size )throw( bad_alloc )
		{
			this->size = size;
			this->arr = new int[ this->size ];
		}
		int Array::getElement(int index )const throw(ArrayIndexOutOfBoundsException)
		{
			if( index < 0 || index > this->size )
			throw ArrayIndexOutOfBoundsException("Invalid index ");
			return this->arr[ index ];
		}
		void Array::setElement(const int index,const int element )throw(ArrayIndexOutOfBoundsException)
		{

			if( index < 0 || index > this->size )
			throw ArrayIndexOutOfBoundsException("Invalid index ");
			this->arr[ index ] = element;
		}
		int Array::getSize( void )const throw( )
		{
			return this->size;
		}

		void Array::setSize( int size )throw( IllegalArgumentException, bad_alloc )
		{
			if( size < 0 )
				throw IllegalArgumentException("Invalid size");
			this->~Array();
			if( size == 0 )
				this->size = 0;
			else
			{
				this->size = size;
				this->arr = new int[ this->size ];
			}
		}

		Array::~Array( void )throw( )
		{
			if( this->arr != NULL )
			{
				delete[] this->arr;
				this->arr = NULL;
			}
		}

}
