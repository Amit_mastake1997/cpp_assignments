/*
 * main.cpp
 *
 *  Created on: 27-Jul-2020
 *      Author: sunbeam
 */

#include<iostream>
#include<string>
using namespace std;
class cylinder {
private:
	double height;
	double radius;
	double volume;
public:
	cylinder() {
		height = 0;
		radius = 0;
		volume = 0;
	}
	double Accept_cylinder(const double height, const double radius) {
		cout << "Enter the cylinder Height: " << endl;
		cin >> this->height;
		cout << "Enter the cylinder Radius: " << endl;
		cin >> this->radius;
		return radius;
	}

	void set_height(const double height)
	{
		this->height = height;
	}

	void set_radius(const double radius)
	{
		this->radius = radius;
	}

	void Calculate_volume()
	{
		double vc = 3.14 *radius*radius*height;
		cout<<"Volume of cylinder is: "<<vc<<endl;
	}

	void get_volume(){
	  return this->Calculate_volume();
	 // return vol;
	}

};
int main()
{
	double height;
	double radius;
	cylinder c;
	c.Accept_cylinder(height,radius);
	//c.Calculate_volume();
	c.get_volume();
	return 0;
}
